package com.example.demo.repository;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer>{

	@Modifying
	@Transactional
	@Query(value = "UPDATE tasks SET status = 1 WHERE id = ?1", nativeQuery = true)
	public void updateTaskDone(int id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE tasks SET status = 0 WHERE id = ?1", nativeQuery = true)
	public void updateTaskUndone(int id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE tasks SET status = 2 WHERE id = ?1", nativeQuery = true)
	public void updateTaskOver(int id);

//（deadline日付絞込,ID昇順）
	public List<Task> findAllByDeadlineBetweenOrderByDeadlineAscIdAsc(Date start, Date end);

//	(deadline日付絞込、content絞込、ID昇順) 全タスク
	public List<Task> findAllByDeadlineBetweenAndContentContainingOrderByDeadlineAscIdAsc(Date start, Date end, String word);


//（deadline日付絞込(end含まない)、stauts絞込、ID昇順）
	public List<Task> findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanOrderByDeadlineAscIdAsc(int status, Date start, Date end);

//（deadline日付絞込(end含まない)、stauts絞込、content絞込、ID昇順）new
	public List<Task> findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanAndContentContainingOrderByDeadlineAscIdAsc(int status, Date start, Date end, String word);

//（deadline日付絞込(end含む)、status絞込、ID昇順)
	public List<Task> findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanEqualOrderByDeadlineAscIdAsc(int status, Date start, Date end);

//（deadline日付絞込(end含む)、status絞込、content絞込、ID昇順) new
	public List<Task> findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanEqualAndContentContainingOrderByDeadlineAscIdAsc(int status, Date start, Date end, String word);
}
