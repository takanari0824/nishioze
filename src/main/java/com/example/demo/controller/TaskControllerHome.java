package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@RequestMapping("/nishioze")
@Controller
public class TaskControllerHome {

	@Autowired
	TaskService taskService;

	@GetMapping()
	public ModelAndView home(@RequestParam(name = "status", required = false) String status, @RequestParam(required = false) Map<String, String> params) throws ParseException {
		String selectedStart = params.get("selectedStart");
		String selectedEnd = params.get("selectedEnd");

		Date start = getStart(selectedStart);
		Date end = getEnd(selectedEnd);

		String word = params.get("selectedWord");
		if(StringUtils.isEmpty(status)) {
			status = params.get("status");
		}

		List<Task> tasks = findTasks(start, end, word, status);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tasks", tasks);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String date = sdf.format(new Date());
		mav.addObject("date", date);
		mav.addObject("status", status);
		mav.addObject("selectedStart", selectedStart);
		mav.addObject("selectedEnd", selectedEnd);
		mav.addObject("selectedWord", word);
		mav.setViewName("/home");
		return mav;
	}

	@PutMapping("/task/{id}/update")
	public ModelAndView updateStatusDone(@PathVariable("id") int id) {
		Task task = taskService.findtask(id);
		int status = task.getStatus();
			if(status == 0) {
				taskService.updateTaskDone(id);
			} else if(status == 1) {
				taskService.updateTaskUndone(id);
			}

		return new ModelAndView("redirect:/nishioze");
	}

	@DeleteMapping("/task/{id}/delete")
	public ModelAndView deleteTask(@PathVariable("id") int id) {
		taskService.deleteTask(id);
		return new ModelAndView("redirect:/nishioze");
	}

	private Date getStartOfToday() throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		String strToday = sdf1.format(new Date());
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date today = sdf2.parse(strToday + " 00:00:00");
		return today;
	}

	private Date getStart(String selectedStart) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strStart = "2010-01-01 00:00:00";
		Date start = sdf.parse(strStart);

		if(!StringUtils.isEmpty(selectedStart)) {
			start = sdf.parse(selectedStart + " 00:00:00");
		}
		return start;
	}

	private Date getEnd(String selectedEnd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strEnd = "3000-01-01 00:00:00";
		Date end = sdf.parse(strEnd);

		if(!StringUtils.isEmpty(selectedEnd)) {
			end = sdf.parse(selectedEnd + " 23:59:59");
		}
		return end;
	}

	private List<Task> findTasks(Date start, Date end, String word, String status) throws ParseException{
		if(StringUtils.isEmpty(status)) {
			status = "all";
		}

		List<Task> tasks = null;
		Date today = getStartOfToday();

		switch(status) {
		case "yet":
			if(today.after(start) || today.equals(start)) {
				start = today;
			}
			if(!StringUtils.isEmpty(word)) {
				tasks = taskService.findAllTaskIncludeEndWithWord(0, start, end, word);
				break;
			}
			tasks = taskService.findAllTaskIncludeEnd(0, start, end);
			break;
		case "done":
			if(!StringUtils.isEmpty(word)) {
				tasks = taskService.findAllTaskIncludeEndWithWord(1, start, end, word);
				break;
			}
			tasks = taskService.findAllTaskIncludeEnd(1, start, end);
			break;
		case "over":
			if(today.before(end)){
				end = today;
				if(!StringUtils.isEmpty(word)) {
					tasks = taskService.findAllTaskExcludeEndWithWord(0, start, end, word);
					break;
				}
				tasks = taskService.findAllTaskExcludeEnd(0, start, end);
				break;
			} else {
				if(!StringUtils.isEmpty(word)) {
					tasks = taskService.findAllTaskIncludeEndWithWord(0, start, end, word);
					break;
				}
				tasks = taskService.findAllTaskIncludeEnd(0, start, end);
				break;
			}
		case "all":
			if(!StringUtils.isEmpty(word)) {
				tasks = taskService.findAllTaskWithWord(start, end, word);
				break;
			}
			tasks = taskService.findAllTask(start, end);
			break;
		}
		return tasks;
	}
}