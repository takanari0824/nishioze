package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@RequestMapping("/nishioze")
@Controller
public class TaskController {

	@Autowired
	TaskService taskService;

	//Getリクエストの処理。新規タスク投稿画面
	@GetMapping("/newtask")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		String content = null;
		String strDeadline = null;

		//翌日の日付を取得
		strDeadline =  getTommorow(strDeadline);

		mav.addObject("content", content);
		mav.addObject("deadline", strDeadline);
		mav.setViewName("/newtask");

		return mav;
	}




	//Postリクエストの処理。新規タスク投稿画面。
	//formに投稿された内容をTask型で受け取る。ステータスは未着手なので0
	@PostMapping("/addtask")
	public ModelAndView createTask(@RequestParam Map<String,String> requestParams) throws ParseException  {

		//バリデーションメッセージを格納
		List<String> errorMessages = new ArrayList<String>();

		String content = requestParams.get("content");
		String strDeadline = requestParams.get("deadline");

		Date deadline = null;

		//日付が登録されて来た場合
		if(!(strDeadline.isEmpty())) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			deadline = format.parse(strDeadline);
		}

		//バリデーションにかかった場合
		if (!isValid(content, deadline, errorMessages)) {

			return returnNewtask(errorMessages, content, strDeadline);
		}


		//バリデーションを無事通過した場合
		Task task = new Task();
		task.setDeadline(deadline);
		task.setContent(content);
		task.setCreatedDate(new Date());
		task.setUpdatedDate(new Date());
		task.setStatus(0);
		taskService.saveTask(task);
		return new ModelAndView("redirect:/nishioze");
	}




	private boolean isValid(String content, Date deadline, List<String> errorMessages) {



		Date now = new Date();

		if (StringUtils.isEmpty(content) || 20 < content.length()) {
            errorMessages.add("タスクは20文字以下で入力してください");
        }

		if(deadline == null) {
			errorMessages.add("日付を入力してください");
		}
		if(deadline != null) {
			if(deadline.before(now)) {
				errorMessages.add("タスクは明日以降で入力してください");
			}
		}


		if (errorMessages.size() != 0) {
            return false;
        }
        return true;
	}

	private ModelAndView returnNewtask( List<String> errorMessages, String content, String strDeadline) {
		ModelAndView mav = new ModelAndView();


		mav.setViewName("/newtask");

		mav.addObject("content", content);

		//日付が空欄だったら翌日以降の日付を再セットする。
		if(strDeadline.isEmpty()) {
			strDeadline =   getTommorow(strDeadline);
		}

		mav.addObject("deadline", strDeadline);
		mav.addObject("errorMessages", errorMessages);

		return mav;

	}

	//翌日の日付取得
	private String getTommorow(String strDeadline) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date deadline = calendar.getTime();
		strDeadline = sdf.format(deadline);

		return strDeadline;
	}

}
