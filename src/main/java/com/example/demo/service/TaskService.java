package com.example.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class TaskService {




	@Autowired
	TaskRepository taskRepository;

	public void saveTask(Task task) {
		taskRepository.save(task);
	}

	public Task findtask(int id) {
		return taskRepository.findById(id).orElse(null);
	}

	public List<Task> findAllTask(Date start, Date end){
		return taskRepository.findAllByDeadlineBetweenOrderByDeadlineAscIdAsc(start, end);
	}

	public List<Task> findAllTaskWithWord(Date start, Date end, String word){
		return taskRepository.findAllByDeadlineBetweenAndContentContainingOrderByDeadlineAscIdAsc(start, end, word);
	}

	public List<Task> findAllTaskExcludeEnd(int status, Date start, Date end){
		return taskRepository.findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanOrderByDeadlineAscIdAsc(status, start, end);
	}

//	new
	public List<Task> findAllTaskExcludeEndWithWord(int status, Date start, Date end, String word){
		return taskRepository.findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanAndContentContainingOrderByDeadlineAscIdAsc(status, start, end, word);
	}

	public List<Task> findAllTaskIncludeEnd(int status, Date start, Date end){
		return taskRepository.findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanEqualOrderByDeadlineAscIdAsc(status, start, end);
	}

//	new
	public List<Task> findAllTaskIncludeEndWithWord(int status, Date start, Date end, String word){
		return taskRepository.findAllByStatusAndDeadlineGreaterThanEqualAndDeadlineLessThanEqualAndContentContainingOrderByDeadlineAscIdAsc(status, start, end, word);
	}

	public void deleteTask(int id) {
		taskRepository.deleteById(id);
		return;
	}

	public void updateTaskDone(int id) {
		taskRepository.updateTaskDone(id);
		return;
	}

	public void updateTaskUndone(int id) {
		taskRepository.updateTaskUndone(id);
	}

	public void updateTaskOver(int id) {
		taskRepository.updateTaskOver(id);
	}
}

