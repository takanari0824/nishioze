package com.example.demo.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.example.demo.entity.Task;

@Component
public class Helper {

	public static boolean beforeDeadline(Task task) {
		Date deadline = task.getDeadline();
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String strDeadline = sdf.format(deadline);
		String strNow = sdf.format(now);
		if(now.before(deadline) || strDeadline.equals(strNow)) {
			return true;
		}
		return false;
	}
}
